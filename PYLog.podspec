
Pod::Spec.new do |s|

  def self.smart_version
    tag = `git describe --abbrev=0 --tags 2>/dev/null`.strip
    if $?.success? then tag else "0.0.1" end
  end

  s.name             = 'PYLog'
  s.version          = smart_version
  s.summary          = 'PYLog.'
  s.description      = <<-DESC
打印库
                       DESC

  s.homepage         = 'https://gitlab.com/bigman-public/PYLog.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'yunhe.lin' => 'bigmanhxx@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/bigman-public/PYLog.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'PYLog/Classes/**/*.*'
  
#s.resource_bundles = {
#   'PYLog' => ['PYLog/Recourse/**/*.*', 'PYLog/Recourse/Images/Media.xcassets']
# }
#
# s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
