//
//  PYLog.swift
//  Pods
//
//  Created by yunhe.lin on 17/2/22.
//
//

import UIKit

/// 设置是否为挑食
public var LogIsDebug = false

public enum PYLog {
    case ln(_: String)
    case url(_: String)
    case error(_: NSError)
    case date(_: NSDate)
    case obj(_: AnyObject)
    case any(_: Any)
}

postfix operator / {}

public postfix func / (_ target: PYLog?) {
    
    guard LogIsDebug else {
        return
    }
    
    guard let target = target else { return }
    
    func log<T>(_ emoji: String, _ object: T) {
        print(emoji + " " + String(describing: object))
    }
    
    switch target {
    case .ln(let line):
        log("✏️", line)
        
    case .url(let url):
        log("🌏", url)
        
    case .error(let error):
        log("❗️", error)
        
    case .any(let any):
        log("⚪️", any)
        
    case .obj(let obj):
        log("◽️", obj)
        
    case .date(let date):
        log("🕒", date)
    }
}
